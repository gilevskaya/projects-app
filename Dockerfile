FROM node:11.2.0

ENV PORT 3000
EXPOSE 3000

WORKDIR /projects-app

RUN npm install serve -g

COPY package.json yarn.lock ./
RUN yarn

COPY . .
RUN yarn build

ENTRYPOINT ["serve", "-p", "3000", "build/client"]