import * as React from "react";

interface IProjectButton {
  type: "edit" | "delete";
  onClick: any;
}

export class ProjectButton extends React.Component<IProjectButton> {
  public render() {
    const buttonStyles = `ProjectButton-${this.props.type}`;

    return (
      <button className="ProjectButton" onClick={this.props.onClick}>
        <div
          className={`h-full w-full bg-cover ProjectButton-delete ${buttonStyles}`}
        />
      </button>
    );
  }
}
