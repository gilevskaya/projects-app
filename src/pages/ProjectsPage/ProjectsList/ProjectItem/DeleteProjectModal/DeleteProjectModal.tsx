import * as React from "react";
import { Button } from "antd";

interface IModalProps {
  onModalNo: any; // function
  onModalYes: any; // function
}

export class DeleteProjectModal extends React.Component<IModalProps> {
  public render() {
    return (
      <div className="DeleteProjectModal fixed pin w-full h-full z-10">
        <div className="DeleteProjectModal-inner relative bg-white w-full max-w-md m-auto flex-col flex flex-col justify-between">
          <div className="flex">
            <div className="DeleteProjectModal-icon bg-cover" />
            <div className="DeleteProjectModal-text">
              <div>Are you sure you want to delete this project?</div>
              <div className="DeleteProjectModal-text-secondary">
                This action can't be undone.
              </div>
            </div>
          </div>
          <div className="flex justify-end">
            <div className="DeleteProjectModal-button">
              <Button onClick={this.props.onModalNo}>No</Button>
            </div>
            <div className="DeleteProjectModal-button">
              <Button onClick={this.props.onModalYes} type="primary">
                Yes
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
