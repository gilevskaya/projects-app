import * as React from "react";
import { connect } from "react-redux";
import {
  IReduxProjectsState,
  IProjectInfo,
  addProject,
  deleteProject,
  editProjectName,
  editProjectOn,
  deleteProjectOn,
  resetAllFlags,
  toggleAddNewProject
} from "../../../../redux/actions";
import * as moment from "moment";
import { Input } from "antd";
import { ProjectButton } from "./ProjectButton/ProjectButton";
import { DeleteProjectModal } from "./DeleteProjectModal/DeleteProjectModal";

const DATE_FORMAT = "MMM D, YYYY[\xa0\xa0\xa0\xa0]h:mma";

interface IProjectItemProps extends IReduxProjectsState {
  type: "show" | "add" | "edit";
  projectId?: number;
  edit: { add: any; delete: any; edit?: any };
  flags: {
    toggleAdd: any;
    editOn: any;
    deleteOn: any;
    resetAll: any;
  };
}

const mapStateToProps = (state: IReduxProjectsState) => {
  return state;
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    edit: {
      delete: (id: number) => {
        return () => dispatch(deleteProject(id));
      },
      add: (name: string) => {
        return () => dispatch(addProject(name));
      },
      edit: (id: number, name: string) => {
        return () => dispatch(editProjectName(id, name));
      }
    },
    flags: {
      toggleAdd: () => {
        return () => dispatch(toggleAddNewProject());
      },
      editOn: (id: number) => {
        return () => dispatch(editProjectOn(id));
      },
      deleteOn: (id: number) => {
        return () => dispatch(deleteProjectOn(id));
      },
      resetAll: () => {
        return () => dispatch(resetAllFlags());
      }
    }
  };
};

class ProjectItem extends React.Component<IProjectItemProps> {
  public render() {
    let projectInfo: IProjectInfo;

    if (this.props.type === "add")
      projectInfo = { id: -1, name: "", createdAt: new Date(0) };
    else {
      const existingProjectInfo = this.props.projects.find(
        (p: IProjectInfo) => p.id === this.props.projectId
      );
      if (!existingProjectInfo)
        throw new Error(`Can't find project with id ${this.props.projectId}`);
      else projectInfo = existingProjectInfo;
    }

    const isExtraHidden = this.props.type === "add" ? "hidden" : "";
    const isProjectNameGroupHidden = this.props.type === "show" ? "" : "hidden";
    const isInputHidden = isProjectNameGroupHidden ? "" : "hidden";
    const showDeleteModal =
      this.props.current &&
      this.props.current.type === "delete" &&
      this.props.current.id === projectInfo.id;

    return (
      <div>
        {showDeleteModal && (
          <DeleteProjectModal
            onModalNo={this.props.flags.resetAll()}
            onModalYes={this.deleteProject()}
          />
        )}
        <div className="ProjectItem w-full flex justify-between items-center">
          <div className="flex justify-start flex items-center">
            <div className="ProjectItem-icon ProjectItem-icon-default bg-cover" />
            <div className="ProjectItem-name-group flex justify-between items-center">
              <div className={isInputHidden}>
                <Input
                  placeholder="Name your project"
                  defaultValue={projectInfo.name}
                  onPressEnter={
                    this.props.type === "add"
                      ? this.addNewProject()
                      : this.editProjectName(this.props.projectId)
                  }
                />
              </div>
              <div className={`ProjectItem-name ${isProjectNameGroupHidden}`}>
                {projectInfo.name}
              </div>
              <div className={isProjectNameGroupHidden}>
                <ProjectButton
                  type="edit"
                  onClick={this.props.flags.editOn(this.props.projectId)}
                />
              </div>
            </div>
          </div>

          <div className={`ProjectItem-date ${isExtraHidden}`}>
            {moment(projectInfo.createdAt).format(DATE_FORMAT)}
          </div>
          <div className={isExtraHidden}>
            <ProjectButton
              type="delete"
              onClick={this.props.flags.deleteOn(this.props.projectId)}
            />
          </div>
        </div>
      </div>
    );
  }

  private addNewProject() {
    return (event: any) => {
      this.props.edit.add(event.target.value)();
      this.props.flags.resetAll()();
    };
  }

  private deleteProject() {
    return () => {
      this.props.edit.delete(this.props.projectId)();
      this.props.flags.resetAll();
    };
  }

  private editProjectName(projectId: number | undefined) {
    return (event: any) => {
      this.props.edit.edit(projectId, event.target.value)();
      this.props.flags.resetAll()();
    };
  }
}

const ProjectItemConnected = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectItem);

export { ProjectItemConnected as ProjectItem };
