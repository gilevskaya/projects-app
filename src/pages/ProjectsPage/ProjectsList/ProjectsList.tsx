import * as React from "react";
import { connect } from "react-redux";
import { IProjectInfo, IReduxProjectsState } from "../../../redux/actions";
import { ProjectItem } from "./ProjectItem/ProjectItem";

type IProjectsListProps = IReduxProjectsState;

const mapStateToProps = (state: IReduxProjectsState) => {
  return state;
};

class ProjectsList extends React.Component<IProjectsListProps> {
  public render() {
    const addTopBorder =
      this.props.projects.length > 0 ||
      (this.props.current && this.props.current.type === "add");

    const ProjectsListComponent = this.props.projects.map((p: IProjectInfo) => (
      <div key={p.id}>
        <ProjectItem
          type={
            this.props.current &&
            this.props.current.type === "edit" &&
            this.props.current.id === p.id
              ? "edit"
              : "show"
          }
          projectId={p.id}
        />
      </div>
    ));

    return (
      <div className="flex flex-col w-full">
        {addTopBorder && <div className="ProjectsList-border" />}
        {this.props.current && this.props.current.type === "add" && (
          <ProjectItem type="add" />
        )}
        {ProjectsListComponent}
      </div>
    );
  }
}

const ProjecstListConnected = connect(mapStateToProps)(ProjectsList);

export { ProjecstListConnected as ProjectsList };
