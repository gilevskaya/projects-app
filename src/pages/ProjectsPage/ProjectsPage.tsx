import * as React from "react";
import { Header } from "../../components/Header/Header";
import { ProjectsList } from "./ProjectsList/ProjectsList";

export class ProjectsPage extends React.Component {
  public render() {
    return (
      <div className="ProjectsPage min-h-screen min-w-screen pb-20">
        <Header title="My Projects" />
        <div className="container">
          <ProjectsList />
        </div>
      </div>
    );
  }
}
