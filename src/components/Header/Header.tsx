import * as React from "react";
import { ActionButton } from "./ActionButton/ActionButton";
import {
  IReduxProjectsState,
  toggleAddNewProject,
  ICurrent
} from "../../redux/actions";
import { connect } from "react-redux";

interface IHeaderProps {
  title: string;
  current: ICurrent;
  toggleAddNewProject: any; // function
}

const mapStateToProps = (state: IReduxProjectsState) => {
  return { current: state.current };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    toggleAddNewProject: () => {
      return () => dispatch(toggleAddNewProject());
    }
  };
};

class Header extends React.Component<IHeaderProps> {
  public render() {
    const isAddOn = this.props.current && this.props.current.type === "add";

    return (
      <div className="Header">
        <div className="Header-inner h-full container">
          <div className="h-full flex flex-col justify-between">
            <div className="Header-logo relative bg-cover" />
            <div className="Header-title uppercase">
              {this.props && this.props.title}
            </div>
          </div>
        </div>
        <div className="container">
          <div className="flex justify-end">
            <ActionButton
              isOn={isAddOn}
              onClick={this.props.toggleAddNewProject()}
            />
          </div>
        </div>
      </div>
    );
  }
}

const HeaderConnected = connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);

export { HeaderConnected as Header };
