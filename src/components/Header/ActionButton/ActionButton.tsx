import * as React from "react";

interface IAactionButtonProps {
  isOn: boolean;
  onClick: any; // function
}

export class ActionButton extends React.Component<IAactionButtonProps> {
  public render() {
    const extraStyles = this.props.isOn ? "ActionButton-icon-on" : "";

    return (
      <div>
        <button onClick={this.props.onClick}>
          <div className="ActionButton relative rounded-full">
            <div
              className={`ActionButton-icon ${extraStyles} bg-no-repeat bg-center w-full h-full`}
            />
          </div>
        </button>
      </div>
    );
  }
}
