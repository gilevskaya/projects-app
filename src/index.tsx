import * as React from "react";
import * as ReactDom from "react-dom";
import history from "./utils/history";
import registerServiceWorker from "./utils/registerServiceWorker";
import { ProjectsPage as AppPage } from "./pages/ProjectsPage/ProjectsPage";

import "./styles/tailwind.css";
import "antd/dist/antd.css";

import { createStore } from "redux";
import { Provider } from "react-redux";
import { rootReducer } from "./redux/reducers";

const store = createStore(rootReducer);
// import logger from "typed-logger";
// store.subscribe(() => logger.info(store.getState()));

export interface IAppProps {
  pathname: string;
}

export interface IAppState {
  pathname: string;
}

const getPage = (pathname: string) => {
  switch (pathname) {
    case "/":
      return AppPage;

    default:
      return AppPage; // ErrorPage 404 should be here
  }
};

export class App extends React.Component<IAppProps, IAppState> {
  constructor(props: IAppProps) {
    super(props);
    this.state = {
      pathname: props.pathname
    };
  }

  public componentDidMount() {
    history.onChange((pathname: string) => {
      this.setState({ pathname });
    });
  }

  public render() {
    const Page = getPage(this.state.pathname);

    return (
      <Provider store={store}>
        <Page />
      </Provider>
    );
  }
}

ReactDom.render(<App pathname={location.pathname} />, document.getElementById(
  "root"
) as HTMLElement);
registerServiceWorker();
