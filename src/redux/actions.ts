let nextProjectId = 2;

export interface IProjectInfo {
  id: number;
  name: string;
  createdAt: Date;
}

export type ICurrent =
  | { type: "edit" | "delete"; id: number }
  | { type: "add" };

export interface IReduxProjectsState {
  projects: IProjectInfo[];
  current?: ICurrent;
}

export const addProject = (name: string) => ({
  type: "PROJECT_ADD",
  id: nextProjectId++,
  name
});

export const deleteProject = (id: number) => ({
  type: "PROJECT_DELETE",
  id
});

export const editProjectName = (id: number, name: string) => ({
  type: "PROJECT_EDIT_NAME",
  id,
  name
});

export const editProjectOn = (id: number) => ({
  type: "CURRENT_SET_EDIT",
  id
});

export const deleteProjectOn = (id: number) => ({
  type: "CURRENT_SET_DELETE",
  id
});

export const toggleAddNewProject = () => ({
  type: "CURRENT_TOGGLE_ADD"
});

export const resetAllFlags = () => ({
  type: "CURRENT_RESET"
});
