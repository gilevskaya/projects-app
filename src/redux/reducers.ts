// import { combineReducers } from "redux";
import { IReduxProjectsState, IProjectInfo } from "./actions";

export const initialState = {
  projects: []
};

const projects = (state: IReduxProjectsState = initialState, action: any) => {
  // ---
  if (action.type === "PROJECT_ADD") {
    const { id, name } = action;
    const newProjects = [...state.projects];
    newProjects.unshift({ id, name, createdAt: new Date() });
    const newState = { ...state };
    newState.projects = newProjects;
    return newState;
    // ---
  } else if (action.type === "PROJECT_DELETE") {
    const { id } = action;
    const newProjects = [...state.projects].filter(p => p.id !== id);
    const newState = { ...state };
    newState.projects = newProjects;
    return newState;
    // ***
  } else if (action.type === "PROJECT_EDIT_NAME") {
    const newState = { ...state };
    const updatedProjects = state.projects.map((p: IProjectInfo) => {
      if (action.id === p.id) p.name = action.name;
      return p;
    });
    newState.projects = updatedProjects;
    return newState;
    // ***
    // *** current
    // ***
  } else if (action.type === "CURRENT_SET_EDIT") {
    const newState = { ...state };
    if (action.id !== undefined)
      newState.current = { type: "edit", id: action.id };
    else delete newState.current;
    return newState;
    // ***
  } else if (action.type === "CURRENT_SET_DELETE") {
    const newState = { ...state };
    if (action.id !== undefined)
      newState.current = { type: "delete", id: action.id };
    else delete newState.current;
    return newState;
    // ***
  } else if (action.type === "CURRENT_TOGGLE_ADD") {
    const newState = { ...state };
    if (newState.current && newState.current.type === "add")
      delete newState.current;
    else newState.current = { type: "add" };
    return newState;
    // ***
  } else if (action.type === "CURRENT_RESET") {
    const newState = { ...state };
    delete newState.current;
    return newState;
    // ***
  } else return state;
};
export const rootReducer = projects;
