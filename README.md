## Projects App

App needs late-ish versions of npm, node and yarn installed.
To start / test run:

```shell
yarn
yarn start
```

App default port is 3000. Or you can specify port with PORT env variable.

You can also start the app with Docker, it's set up for prod.
